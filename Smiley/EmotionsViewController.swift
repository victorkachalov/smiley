//
//  EmotionsViewController.swift
//  Smiley
//
//  Created by Viktor Kachalov on 05.08.17.
//  Copyright © 2017 Viktor Kachalov. All rights reserved.
//

import UIKit

class EmotionsViewController: VCLLoggingViewController {


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationViewController = segue.destination
        if let navigationController = destinationViewController as? UINavigationController {
            destinationViewController = navigationController.visibleViewController ?? destinationViewController
        }
        if let faceViewController = destinationViewController as? FaceViewController,
            let identifier = segue.identifier,
                let expression = emotionalFaces[identifier] {
                    faceViewController.expression = expression
                    faceViewController.navigationItem.title = (sender as? UIButton)?.currentTitle
                }
    }

    private let emotionalFaces: Dictionary<String,FacialExpression> = [
        "sad": FacialExpression(eyes: .closed, mouth: .frown),
        "happy": FacialExpression(eyes: .open, mouth: .smile),
        "worried": FacialExpression(eyes: .open, mouth: .smirk)
    ]
    
}
