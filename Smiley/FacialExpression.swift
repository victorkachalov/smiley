//
//  FacialExpression.swift
//  Smiley
//
//  Created by Viktor Kachalov on 01.08.17.
//  Copyright © 2017 Viktor Kachalov. All rights reserved.
//

import Foundation

struct FacialExpression { //определям структуру выражений лица
    
    enum Eyes: Int { //выражения для глаз (перечисления)
        case open
        case closed
        case squinting
    }
    
    enum Mouth: Int { //выражения для рта (перечисления)
        case frown
        case smirk
        case neutral
        case grin
        case smile
        
        var sadder: Mouth { 
            return Mouth(rawValue: rawValue - 1) ?? .frown
        }
        var happier: Mouth {
            return Mouth(rawValue: rawValue + 1) ?? .smile
        }
        
    }
    
    var sadder: FacialExpression {
        return FacialExpression(eyes: self.eyes, mouth: self.mouth.sadder)
    }
    var happier: FacialExpression {
        return FacialExpression(eyes: self.eyes, mouth: self.mouth.happier)
    }
    
    let eyes: Eyes
    let mouth: Mouth
    
}
